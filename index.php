<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<?php
session_start();
?>

<?php

	include 'koneksi.php';
 
	$query = mysqli_query($koneksi, "SELECT max(id) as idterbesar FROM komentar");
	$data = mysqli_fetch_array($query);
	$id = $data['idterbesar'];
 
	$urutan = (int) substr($id, 1, 1);
 
	$urutan++;
  
	$angka = "";
	$id = $angka . sprintf("%01s", $urutan);
?>

<html lang="en-US">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Wedding Dian & Arif</title>
    <meta name="description" content="true"/>
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <link href="https://fonts.googleapis.com/css?family=Dosis:400,500" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.4/js/all.js"></script>
    <link href="css/aos.css" rel="stylesheet">
    <link href="css/ekko-lightbox.css" rel="stylesheet">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link href="styles/mainn.css" rel="stylesheet">
  </head>

  <?php
	$url = 'https://www.google.com/calendar/render?action=TEMPLATE&text={{ text }}&dates={{ start }}/{{ end }}&details={{ details }}&location={{ location }}&sf=true&output=xml';
	
	$dateStr = '2021-1-27T07:00:00';
	$timezone = 'Asia/Jakarta';
	$dtUtcDate = strtotime($dateStr. ' '. $timezone);
	$dtUtcEndDate = strtotime($dateStr. '+ 9 Hours'. $timezone);
	$event = [
		'text' => 'Dian & Arif Wedding By Official Dailys',
		'start' => date('Ymd\THis\Z', $dtUtcDate),
		'end' => date('Ymd\THis\Z', $dtUtcEndDate),
		'details' => '',
		'location' => ''
	];

	foreach ($event as $key => $value) {
		$url = str_replace("{{ $key }}", urlencode($value), $url);
	}
  ?>
  
  <?php
	$urls = 'https://www.google.co.id/maps/place/Jl.+Griya+Citra+Persada+Blok+W+No.13,+RT.12%2FRW.5,+Dawuan+Tim.,+Kec.+Cikampek,+Kabupaten+Karawang,+Jawa+Barat+41373/@-6.3963303,107.4414627,18z/data=!3m1!4b1!4m5!3m4!1s0x2e69725748024a17:0x504dfc50e38e460a!8m2!3d-6.396333!4d107.442557';
	?>

  <body id="top">
    <header>
      <script src="https://cdn.rawgit.com/topuler/widget/f9185dfc/efek-salju.js" type="text/javascript"></script>
    </header>
    <div class="page-content">
      <div class="div">
<div class="ww-home-page" id="home">
  <div class="ww-wedding-announcement d-flex align-items-center justify-content-start">
    <div class="container ww-announcement-container">
      <p class="h2 mt-5 ww-title" data-aos="zoom-in-down" data-aos-delay="300" data-aos-duration="1000" data-aos-offset="10">The Wedding Of</p>
      <br/>
      <br/>
      
      <p class="ww-couple-name ww-title" data-aos="zoom-in-down" data-aos-delay="300" data-aos-duration="1000">Dian & </p>
      <p class="ww-couple-name ww-title" data-aos="zoom-in-down" data-aos-delay="300" data-aos-duration="1000">Arif</p>
      <!-- <img class="img-fluid " height="40" width="40" src="images/laurel-1.png" alt="Heart" data-aos="zoom-in-down" data-aos-delay="300" data-aos-duration="1000"/> -->
      <p class="h2 mt-5 ww-title" data-aos="zoom-in-down" data-aos-delay="300" data-aos-duration="1000" data-aos-offset="10">27<sup>th</sup> Januari, 2021</p>
      <p class="h2 mt-5 ww-title" data-aos="zoom-in-down" data-aos-delay="300" data-aos-duration="1000" data-aos-offset="10">Wedding Announcement</p>
      </br>
      <p class="simply-countdown simply-countdown-one" data-aos="zoom-in-down" data-aos-delay="300" data-aos-duration="1000" data-aos-offset="10"></p>				
    </div>
  </div>
</div>

<div class="ww-section bg-light" id="couple">
  <div class="container">
  <img class="mx-auto d-block" src="images/bunga.png" style="float:middle;width:250px;height:70px; alt="Heart" data-aos="zoom-in-down" data-aos-delay="300" data-aos-duration="1000"/>
  <br/>  
  
  <h2 class="h1 text-center pt-3 ww-title" data-aos="zoom-in-down" data-aos-duration="1000">Assalamualaikum Warahmatullahi Wabarakatuh</h2>  
  <p class="p text-center "> Dengan memohon rahmat dan ridho Allah SWT</p>
  <p class="p text-center "> Kami bermaksud menyelenggarakan resepsi pernikahan kami</p>
    <hr width="100%" noshade size="20%">
    <br/>
    <div class="row text-center">
      <div class="col-md-12">
        <!-- <div class="mt-3"><img class="img-fluid img-thumbnail" src="images/bride.jpg" height="400" width="400" alt="Bride" data-aos="flip-left" data-aos-duration="1000"/> -->
          <br/>
          <br/>
          <h2 class="h1 text-center ww-title">Dian Indriyani Dewi</h2>
          <p class="pt-3 text-center text-muted"><i>Putri Bapak Acam & Ibu Didah Apidah</i></p>
          <br/>
          <br/>
          <h2 class="h2 text-center ww-title">Dengan</h2>
          <br/>
          <br/>
          <h2 class="h1 text-center ww-title">Arif Mulyo Aji</h2>
          <p class="pt-3 text-center text-muted"><i>Putra Bapak Siyamin & Ibu Susinayah (Almh)</i></p>
          <!-- <div class="ww-social-links"><a class="btn btn-link" href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a><a class="btn btn-link" href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a><a class="btn btn-link" href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a><a class="btn btn-link" href="#"><i class="fab fa-instagram" aria-hidden="true"></i></a></div> -->
        <!-- </div> -->
      </div>
    </div>
    <br/>
    <hr width="100%" noshade size="20%">
    <br/>
    <p class="p text-center">وَمِنۡ اٰيٰتِهٖۤ اَنۡ خَلَقَ لَكُمۡ مِّنۡ اَنۡفُسِكُمۡ اَزۡوَاجًا لِّتَسۡكُنُوۡۤا اِلَيۡهَا وَجَعَلَ بَيۡنَكُمۡ مَّوَدَّةً وَّرَحۡمَةً  ؕ اِنَّ فِىۡ ذٰ لِكَ لَاٰيٰتٍ لِّقَوۡمٍ يَّتَفَكَّرُوۡنَ</p>
    <p class="p text-center "> Dan di antara tanda-tanda (kebesaran)-Nya ialah Dia menciptakan pasangan-pasangan untukmu dari jenismu sendiri, agar kamu cenderung dan merasa tenteram kepadanya, dan Dia menjadikan di antaramu rasa kasih dan sayang. Sungguh, pada yang demikian itu benar-benar terdapat tanda-tanda (kebesaran Allah) bagi kaum yang berpikir.</p>
    <br/>
    <h3 class="h2 ww-title text-center">Q.S. Ar - Ruum :21</h3>
  </div>
</div>

<div class="ww-section" id="people">
  <div class="container ww-couple-friends">
  <img class="mx-auto d-block" src="images/bunga.png" style="float:middle;width:250px;height:70px; alt="Heart" data-aos="zoom-in-down" data-aos-delay="300" data-aos-duration="1000"/>
  <br/>   
  <h2 class="h1 text-center pb-3 ww-title" data-aos="zoom-in-down" data-aos-duration="1000">Bismillahirahmanirrahim</h2>
  <br/>
    <p class="p text-center">Sebagai rasa syukur atas berkat dan kemurahan kasih-Nya, kami mengundang Bapak/Ibu/Saudara/i untuk menghadiri Resepsi Pernikahan kami yang insya allah diselenggarakan pada:</p>
  <hr width="100%" noshade size="20%"> 
  <br/>
    <h2 class="h2 ww-title text-center">Rabu, 27 Januari 2021</h2>
  <br/>
    <img class="mx-auto d-block" height="200" width="200" src="images/sources.gif" alt="Heart" data-aos="zoom-in-down" data-aos-delay="300" data-aos-duration="1000"/>
  <br/>
  <br/>
    <h2 class="h2 ww-title col text-center"><a href="<?php echo $url?>" target="__blank" class="btn btn-primary btn-submit">Save To Calender</a></h2>  
  <br/>
  <br/>
  <br/>
    <h2 class="h2 ww-title text-center">Waktu Akad</h2>
    <p class="p text-center">09:00 WIB - 10:00 WIB</p>
  <hr width="75%" noshade size="20%">
  <br/>
  <br/>
  <br/>
    <h2 class="h2 ww-title text-center">Resepsi Pernikahan</h2>
    <p class="p text-center">10:00 WIB - 17:00 WIB</p>
  <hr width="75%" noshade size="20%">
  <br/>
  <br/>
  <br/>
    <h2 class="h2 ww-title text-center">Bertempat di</h2>
    <p class="p text-center">Kediaman mempelai wanita</p>
  <p class="p text-center">Perum Griya Citra Persada Blok W No. 13 Rt02/10 Ds. Dawuan Timur - Cikampek</p>
  <br/>
  <br/>
    <h2 class="h2 ww-title col text-center"><a href="<?php echo $urls?>" target="__blank" class="btn btn-primary btn-submit">View Map</a></h2>
  <br/>
  <br/>
    <p class="p text-center">Tanpa mengurangi rasa hormat, untuk mematuhi peraturan dalam upaya pencegahan Covid-19 maka kami menerapkan protokol kesehatan di acara ini,
    diharapkan agar Bapak/Ibu/Saudara/i dapat mengunakan masker atau face shield dan tetap menjaga jarak selama acara berlangsung.
    </p>
  <hr width="100%" noshade size="20%">
  <br/>
  <br/>
  <p class="p text-center">Merupakan suatu kehormatan dan kebahagiaan bagi kami apabila Bapak/Ibu/Saudara/i berkenan hadir dan memberikan doa restu
  </p>
  <p class="p text-center"> Atas kehadiran dan doa restunya kami ucapkan terimakasih.
  </p>
  <br/>
  <br/>
  <h2 class="h1 text-center pt-3 ww-title" data-aos="zoom-in-down" data-aos-duration="1000">Wassalamualaikum Warahmatullahi Wabarakatuh</h2>
  
</div>
</div>

<div class="ww-section bg-light" id="gallery">
  <div class="ww-photo-gallery">
    <div class="container">
      <img class="mx-auto d-block" src="images/bunga.png" style="float:middle;width:250px;height:70px; alt="Heart" data-aos="zoom-in-down" data-aos-delay="300" data-aos-duration="1000"/>
      <br/>
      <br/>
      <h3 class="h2 ww-title text-center" data-aos="zoom-in-down" data-aos-duration="1000">Photo Gallery</h3>
      <h2 class="h1 text-center pb-3 ww-title" data-aos="zoom-in-down" data-aos-duration="1000">Dian & Arif</h2>
      <!-- <div class="col-md-12 text-center ww-category-filter mb-4"><a class="btn btn-outline-primary ww-filter-button" href="#" data-filter="all">All</a><a class="btn btn-outline-primary ww-filter-button" href="#" data-filter="vacation">Our Vacation</a><a class="btn btn-outline-primary ww-filter-button" href="#" data-filter="party">Party</a><a class="btn btn-outline-primary ww-filter-button" href="#" data-filter="ceremony">Ceremony</a><a class="btn btn-outline-primary ww-filter-button" href="#" data-filter="wedding">Wedding</a></div> -->
      <div class="ww-gallery" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-duration="1000" data-aos-offset="0">
        <div class="card-columns">
          <div class="card" ><a href="images/bb.jpeg" data-toggle="lightbox" data-gallery="ww-gallery"><img class="img-fluid" src="images/bb.jpeg" alt="Gallery Pic 1"/></a></div>
          <div class="card" ><a href="images/ba.jpeg" data-toggle="lightbox" data-gallery="ww-gallery"><img class="img-fluid" src="images/ba.jpeg" alt="Gallery Pic 2"/></a></div>
          <div class="card" ><a href="images/bc.jpeg" data-toggle="lightbox" data-gallery="ww-gallery"><img class="img-fluid" src="images/bc.jpeg" alt="Gallery Pic 3"/></a></div>
          <!-- <div class="card" ><a href="images/bg.jpeg" data-toggle="lightbox" data-gallery="ww-gallery"><img class="img-fluid" src="images/bg.jpeg" alt="Gallery Pic 4"/></a></div> -->
          <div class="card" ><a href="images/bo.jpeg" data-toggle="lightbox" data-gallery="ww-gallery"><img class="img-fluid" src="images/bo.jpeg" alt="Gallery Pic 5"/></a></div>
          <div class="card" ><a href="images/bt.jpeg" data-toggle="lightbox" data-gallery="ww-gallery"><img class="img-fluid" src="images/bt.jpeg" alt="Gallery Pic 6"/></a></div>
          <div class="card" ><a href="images/bu.jpeg" data-toggle="lightbox" data-gallery="ww-gallery"><img class="img-fluid" src="images/bu.jpeg" alt="Gallery Pic 7"/></a></div>
          <div class="card" ><a href="images/bs.jpeg" data-toggle="lightbox" data-gallery="ww-gallery"><img class="img-fluid" src="images/bs.jpeg" alt="Gallery Pic 8"/></a></div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
	include "koneksi.php";
	$query = mysqli_query($koneksi,"SELECT * FROM komentar ORDER BY tanggal DESC");
	?>

<div class="ww-section" id="people">
<div class="container ww-couple-friends">
    <div class="container">
      <img class="mx-auto d-block" src="images/bunga.png" style="float:middle;width:250px;height:70px; alt="Heart" data-aos="zoom-in-down" data-aos-delay="300" data-aos-duration="1000"/>
      <br/>
      <br/>
      <h2 class="h1 text-center pb-3 ww-title" data-aos="zoom-in-down" data-aos-duration="1000">Doa & Harapan</h2>
      <!-- <div class="col-md-12 text-center ww-category-filter mb-4"><a class="btn btn-outline-primary ww-filter-button" href="#" data-filter="all">All</a><a class="btn btn-outline-primary ww-filter-button" href="#" data-filter="vacation">Our Vacation</a><a class="btn btn-outline-primary ww-filter-button" href="#" data-filter="party">Party</a><a class="btn btn-outline-primary ww-filter-button" href="#" data-filter="ceremony">Ceremony</a><a class="btn btn-outline-primary ww-filter-button" href="#" data-filter="wedding">Wedding</a></div> -->
      <div class="row">
					<div class="col-md-12 animate-box">
						<div class="wrap-testimony"> 
							<div class="owl-carousel-fullwidth">
							<?php if(mysqli_num_rows($query)>0){ ?>
							<?php
								$id = 1;
								while($data = mysqli_fetch_array($query)){
								$nama = $data['nama'];
								$komentar = $data['komentar'];
								$tanggal = $data['tanggal'];
							?>
								<div class="item">
									<div class="testimony-slide active text-center">
									
										<b><?php echo $data['nama']; ?></b>
										<span class="date"><?php 
										setlocale (LC_TIME, 'id_ID');
										echo strftime("%A, %d %B %Y", strtotime($tanggal)). "\n"; ?></span>
										<blockquote>
											<p><i>"<?php echo $data['komentar']; ?>"</i></p>
										</blockquote>
									</div>
								</div>
							<?php $id++; } ?>
							<?php } ?>	
							</div>
						</div>
					</div>
				</div>
    </div>
  </div>
</div>

<div class="ww-section bg-light" id="rsvp">
  <div class="container" data-aos="zoom-in-up" data-aos-duration="1000">
    <div class="col text-center">
    <img class="mx-auto d-block" src="images/bunga.png" style="float:middle;width:250px;height:70px; alt="Heart" data-aos="zoom-in-down" data-aos-delay="300" data-aos-duration="1000"/>
      <br/>
      <br/>
      <h3 class="h2 ww-title text-center" data-aos="zoom-in-down" data-aos-duration="1000">Ucapan Untuk</h3>
      <h2 class="h1 ww-title pb-3" data-aos="zoom-in-down" data-aos-duration="1000">Dian & Arif</h2>
    </div>
    <div class="row ww-rsvp-form">
      <div class="col-md-12">
        <div class="card-body">
          <form action="simpan.php" method="POST">
            <div class="row">
              <div class="col md-6 pb-3">
                <div class="form-group">
                  <label for="name-input">Your Name*</label>
                  <input class="form-control" id="nama" type="text" name="nama" required="required"/>
                </div>
              </div>
              <textarea name="tanggal" type="datetime-local" style="display:none"><?php echo date("Y-m-d");?></textarea>
            </div>
            <!-- <div class="row">
              <div class="col md-6 pb-3">
                <div class="form-group">
                  <label for="guest-input">Number of Guests</label>
                  <select class="form-control" id="guest-input" name="guest">
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                    <option value="4">Four</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6 pb-3">
                <div class="form-group">
                  <label for="events-input">I am Attending</label>
                  <select class="form-control" id="events-input" name="events">
                    <option value="all-event">All Events</option>
                    <option value="wedding-ceremony">Wedding ceremony</option>
                    <option value="reception-party">Reception Party</option>
                  </select>
                </div>
              </div>
            </div> -->
            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label for="message-input">Your Message</label>
                  <textarea class="form-control" id="komentar" name="komentar" rows="5" required="required"/></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col text-center">
                <button class="btn btn-primary btn-submit" type="submit">Send</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="ww-footer bg-light">
  <div class="container text-center py-4">
    <p class="mb-0">Design by - <a class="credit" href="http://Officialdailys.com/" target="_blank">Officialdailys.com</a></p>
  </div>
</div></div>
    </div>
    <footer></footer>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="owlcarousel/dist/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="js/aos.js"></script>
    <script src="js/ekko-lightbox.min.js"></script>
    <script src="scripts/main.js"></script>
    <!-- <script src="js/simplyCountdown.js"></script>
    <script src="js/owl.carousel.min.js"></script> -->

    <script src="js/jquery.min.js"></script>
	  <!-- jQuery Easing -->
    <script src="js/jquery.easing.1.3.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Waypoints -->
    <script src="js/jquery.waypoints.min.js"></script>
    <!-- Carousel -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- countTo -->
    <script src="js/jquery.countTo.js"></script>

    <!-- Stellar -->
    <script src="js/jquery.stellar.min.js"></script>
    <!-- Magnific Popup -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/magnific-popup-options.js"></script>

    <!-- // <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/0.0.1/prism.min.js"></script> -->
    <script src="js/simplyCountdown.js"></script>
    <!-- Main -->
    <script src="js/main.js"></script>



    <script>
    var d = new Date(new Date().getTime() + 200 * 120 *120 * 2000);
    var t = new Date("Jan 27, 2021 08:00:00").getTime(); 
    //var t = deadline - d;
      // default example
      simplyCountdown('.simply-countdown-one', {
          year: 2021,
          month: 1,
          day: 27,
      words: { //words displayed into the countdown
                  days: 'Hari',
                  hours: 'Jam',
                  minutes: 'Menit',
                  seconds: 'Detik',
                  pluralLetter: ' '
              },
      });

      //jQuery example
      $('#simply-countdown-losange').simplyCountdown({
          year: d.getFullYear(),
          month: d.getMonth() + 1,
          day: d.getDate(),
          enableUtc: false
      });

      $(document).ready(function(){
        $('.slide-testi').owlCarousel({
          items: 3,
          loop: true,
          nav: false,
          dots: true,
          autoPlay: true,
          smartSpeed: 800,
          autoHeight: true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:5
                }
            }
        })
      });
    </script>

  </body>
</html>